﻿<?php
ob_start();
include 'conexao.php';
$msg = $_REQUEST["msg"];
include "conexao.php";
//Praticamente faço as mesmas validações que fizemos para o cadastrado do usuário no banco de dados.
//Recebendo os dados e tratando os mesmos para inserção no banco
$recebeNomeUsuario = filter_input(INPUT_POST, 'nomeUsuario', FILTER_SANITIZE_SPECIAL_CHARS);
$confereNomeUsuario = filter_input(INPUT_POST, 'nomeUsuario', FILTER_SANITIZE_MAGIC_QUOTES);
$recebeSenha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS);

//Nesse if, faço uma conferência em relação à senha informada. Se não for informada nenhuma, retorno a mensagem para que o usuário informe algo!
if ($recebeSenha == NULL ) {
echo "<p>A senha precisa ser informada!";
echo "<p><a href='javascript:history.back();'>Voltar</a></p>";
return false;
}

//Aqui faço a segunda parte da verificação: vejo se no nome de usuário foi utilizado algum caractere especial
//Isso serve para evitar uma possível invasão sql no banco de dados, possibilitando assim a proteção e integridade dos dados
//Nesse caso, eu comparo os nomes. Se forem iguais, após passarem pelos filtros, eu inicio a criptografia. Se não forem, peço que volte à página anterior
else if ($confereNomeUsuario != $recebeNomeUsuario) {
echo "<p>Você informou o seguinte Nome de Usuário: <strong>$recebeNomeUsuario</strong> .</p>";
echo "<p>Por favor, não utilize caracteres especiais (tais como aspas simples ou duplas, assim como barras!) no campo <strong>Nome de Usuário</strong>.</p>";
echo "<p><a href='javascript:history.back();'>Volte</a> para a página anterior e tente novamente! Obrigado!</p>";
return false;

} else {

//Aqui vamos criar a função que vai criptografar os dados.
//Serão necessários criptografar apenas o endereço de e-mail e a senha informada

//Função para criptografar a senha
function criptoSenha($criptoSenha){
return sha1(md5($criptoSenha));
}

//Função para criptografar o e-mail
function criptoNomeUsuario($criptoNomeUsuario){
return sha1(md5($criptoNomeUsuario));
}

//Aqui realizo a criptografia do nome de usuário
$criptoNomeUsuario = criptoNomeUsuario(filter_input(INPUT_POST, 'nomeUsuario', FILTER_SANITIZE_MAGIC_QUOTES));

//Aqui realizo a criptografia da senha informada do usuário
$criptoSenha = criptoSenha(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS));

/*
Agora vamos consultar no banco de dados para ver se existe realmente esse cadastro
Vamos verificar ambos os dados: Nome de Usuário e Senha e ainda se o campo "ATIVO" está setado como SIM
Penso eu que, ao invés de simplesmente DELETAR um USUÁRIO ou DELETAR QUALQUER INFORMAÇÃO GRAVADA NO BANCO é tiro no pé.
Vez ou outra, pode ser que seja necessária rever a informação ou a ação, e se tivermos apagado os dados, não temos muito o que fazer.
Lembre-se sempre: o usuário quer uma coisa agora, e outra daqui a 10 minutos. Então, preserve-se da melhor maneira possível.
Prefiro sempre trabalhar com a hipótese de ATIVO, EXIBIR, LIBERAR algo, para poder mudar simplesmente de SIM P/ NÃO e vice-versa, do que deletar a informação.
Posso até IMPRIMIR no botão "Deseja deletar a informação"? mas nunca, realmente nunca, apagar determinada linha ou informação do banco.
*/

$consultaInformacoes = mysql_query("SELECT * FROM usuarios WHERE userlogin = '$criptoNomeUsuario' AND passlogin = '$criptoSenha' AND ativo = 'sim'") or die (mysql_error());
$verificaInformacoes = mysql_num_rows($consultaInformacoes);

//Aqui vou verificar se houve resultado positivo na pesquisa
if($verificaInformacoes == 1){

//Aqui eu vou setar os cookies para gravar os dados de acesso do usuário
setcookie ("login", $criptoNomeUsuario, (time()+60*60*24*30 ));
setcookie ("senha", $criptoSenha, (time()+60*60*24*30 ));
setcookie ("nomeUsuario", $confereNomeUsuario, (time()+60*60*24*30 ));
//header ("Location: conteudoExclusivo.php");

echo "<p>Os dados informados estão corretos!</p>
<p>Por favor, aguarde alguns segundos. Estamos processando a sua entrada!</p>
<p>Obrigado!</p>";

$conteudoExclusivo = "Home.php";
echo "<meta http-equiv=\"refresh\" content=\"3;URL=".$conteudoExclusivo."\">";

} else {
echo "<p>Nome de Usuário ou Senha informada não confere. Por favor, <a href='javascript:history.back();'>volte</a> e tente novamente!</p>";
}

}

?>