﻿<?php
include "conexao.php";
include "infouser.php";
include "buscausuario.php";
include "acessoUsuario.php";
session_start();
$start = filter_input(INPUT_POST, 'start');
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
$senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS);
$user = buscaUsuario($conexao, $email, $senha);
  if(isset($user)){
    $_SESSION['nomeUser'] = $user['nome'];
    $_SESSION['id'] = $user['id_usuario'];
    $_SESSION['emailUser'] = $user['email'];
    $_SESSION['started'] = $user['start'];
  }
  if($_SESSION['started'] == '1'){
    header("Location: welcomeuser.php");
  }
  elseif($_SESSION['started'] == '0'){
    header("Location: start.php");
  }
  else{
    echo "Informe um EMAIL ou SENHA válidos.";
  }

?>
  <a href="javascript:history.back()">voltar</a>
