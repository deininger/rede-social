<?php
include 'conexao.php';
include "acessoUsuario.php";
include "header.php";
include 'infouser.php';
include 'search.php';
logouser();

?>
<html>
<head>
<meta charset="utf-8">
<script language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.min.1.9.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="jquery-1.8.0.min.js"></script>
<script>
	$(document).ready(function(){
	$( "#imgpub" ).click(function() {
  $( "#publicacao" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});
		});
</script>
<style>
div.corpofotoperfil ul {
	list-style: none;
	text-decoration: none;
	font-size: 10pt;
	color: rgba(196,196,196,1.00);
	margin-left: 0%;
	margin-top: 6%;
	position: fixed;
}

div.corpofotoperfil li {
	display: inline-block;
	padding: 2px;
	margin-left: px;
	margin-top: 1%;
}

#perfilnome1 {
    text-align: center;
    position: absolute;
    margin-top: 25px;
    margin-left: 65px;
  	font-size: 12pt;
  	color: #ffffff;
  	text-shadow: 0.1em 0.1em 3px #000;
  }

.corpofotoperfil p {
	font-size: 12pt;
	color: rgba(196,196,196,1.00);
	float: left;
	margin-left: -5px;
	margin-top: 50px;

}

	#line1 {
		width: 653px;
		background: #AFAFAF;
		height: auto;
		float: left;
		margin-top: -12%;
		margin-left: 21%;
		margin-bottom: 1%;
		background: #AFAFAF;

		border-radius: 5px;
}
	#imgpub {
		width: 7%;
		height: 14%;
		position: fixed;
		top: 65%;
		left: 65%;


	}

	div#publicacao form textarea {
		width: 90%;
		margin-top: 1%;
		border: none;
		border-radius: 10px;
		margin-left: 5%;
		float: left;

	}

	div#publicacao form input {
		float: right;
		margin-right: 5%;
		margin-top: 3%;

	}

	div#publicacao {
		width: 20%;
		height: 20%;
		background: rgba(192,192,192,0.60);
		border-radius: 10px;
		position: absolute;
		margin-top: 0%;
		margin-left: 35%;
		display: none;


	}

	#fotomaq {
		width: 20px;
		height: 30px;
		margin-left: -90%;
		margin-top: 36%;
	}

	.corpofotoperfil {
		width: 200px;
		height: 200px;
		background-color: #F8F8FF;
		margin-left: 2%;
		margin-top: 3%;
		position: relative;
		border-radius: 5px;
		-webkit-box-shadow: 0px 6px 13px -2px rgba(0,0,0,0.75);
		opacity: 0;
		-webkit-transition: 5s;

}
	.corpofotoperfil:hover {
		-webkit-transition: 1s;
		opacity: 1;

	}

	.capamini {
		z-index: 0;
		width: 200px;
		height: 80px;
		border-radius: 5px 5px 0px 0px;
		-webkit-box-shadow: 0px 6px 13px -2px rgba(0,0,0,0.75);

}

	.fotoperfil {
		z-index: 2;
		float: left;
		width: 50px;
		height: 50px;
		margin-top: 5%;
		margin-left: 10px;
		border-radius: 50%;

}
</style>
</head>
<body>
<div class="corpofotoperfil">

<img class="capamini" src="upload/<?php echo $_SESSION['capaP']; ?>"/>
<p id="perfilnome1"><?php echo $_SESSION['nomeUser']; ?></p>
<img class="fotoperfil" src="upload/<?php echo $_SESSION['fotoC']; ?>"/>
<ul>
	<a href="perfilusuario.php"><li>Perfil</li></a>
	<li>Amigos</li>
	<li>Fotos</li>
</ul>
</div>
<h1 style="text-align: center; color: #ffffff; position: absolute; text-shadow: 0.1em 0.1em 3px #000; float: left; margin-left: 450px; top: 30px;">Usuários encontrados</h1>
<div id="line1">
	<?php buscar($conexao); ?>
</div>
<div id="publicacao">
	<form method="post"  enctype="multipart/form-data">
		<textarea placeholder="Escreva uma nova publicação..." name="texto"></textarea>
		<label for="file-input">
		<img id="fotomaq" type="image" alt="image" title="Inserir uma foto" src="_imagens/camera.png"/>
		</label>
		<input type="image" name="publish"/>
		<input type="file" id="file-input" name="file" hidden/>
	</form>
</div>
</body>
</html>
