﻿<?php
include "conexao.php";

//Recebendo os dados e tratando os mesmos para inserção no banco
$id = $_POST['id'] = '0';
$recebeSeuNome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_SPECIAL_CHARS);
$confereSeuNome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_MAGIC_QUOTES);
$recebeNomeUsuario = filter_input(INPUT_POST, 'nomeUsuario', FILTER_SANITIZE_SPECIAL_CHARS);
$confereNomeUsuario = filter_input(INPUT_POST, 'nomeUsuario', FILTER_SANITIZE_MAGIC_QUOTES);
$recebeEmail = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$recebeSenha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS);

if ($recebeEmail == NULL ) {
echo "<p>Retorne e digite um e-mail válido por favor!";
echo "<p><a href='javascript:history.back();'>Voltar</a></p>";
return false;
}

if ($recebeSenha == NULL ) {
echo "<p>Retorne e informe uma senha por favor!";
echo "<p><a href='javascript:history.back();'>Voltar</a></p>";
return false;
}

else if ($confereNomeUsuario != $recebeNomeUsuario) {
echo "<p>Você informou o seguinte Nome de Usuário: <strong>$recebeNomeUsuario</strong> .</p>";
echo "<p>Por favor, não utilize caracteres especiais (tais como aspas simples ou duplas, assim como barras!) no campo <strong>Nome de Usuário</strong>.</p>";
echo "<p><a href='javascript:history.back();'>Volte</a> para a página anterior e tente novamente! Obrigado!</p>";
return false;

}
else if ($confereSeuNome != $recebeSeuNome) {
echo "<p>Você informou o Seu Nome como: <strong>$confereSeuNome</strong> .</p>";
echo "<p>Por favor, não utilize caracteres especiais (tais como aspas simples ou duplas, assim como barras!) no campo <strong>Seu Nome</strong>.</p>";
echo "<p><a href='javascript:history.back();'>Volte</a> para a página anterior e tente novamente! Obrigado!</p>";
return false;
}

else {

echo "<h3>Cadastrando informações em nosso banco de dados</h3>";

//Função para criptografar a senha
function criptoSenha($criptoSenha){
return sha1(md5($criptoSenha));
}
//Função para criptografar o e-mail
function criptoNomeUsuario($criptoNomeUsuario){
return sha1(md5($criptoNomeUsuario));
}

$criptoNomeUsuario = criptoNomeUsuario(filter_input(INPUT_POST, 'nomeUsuario', FILTER_SANITIZE_MAGIC_QUOTES));
$criptoSenha = criptoSenha(filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS));

$verificaBanco = mysqli_query($conexao, "SELECT * FROM usuarios WHERE email = '$recebeEmail'");
$consultaBanco = mysqli_fetch_row($verificaBanco);

if ($consultaBanco == true){
  echo "<p>Prezado(a) <strong>$confereSeuNome</strong>, o endereço de e-mail informado (<strong><em>$recebeEmail</em></strong>) já consta em nossa base de dados!</p>";
  echo "<p><a href='javascript:history.back();'>Volte</a> para a página anterior e informe um novo endereço! Obrigado!</p>";
  return false;

}

$InserirDados = mysqli_query($conexao, "INSERT INTO usuarios (id_usuario, nome, email, userlogin, passlogin, ativo, start) VALUES ('$id', '$confereSeuNome', '$recebeEmail', '$criptoNomeUsuario', '$criptoSenha','sim','0')");
if($InserirDados == true){
    echo "cadastro realizado com sucesso! Redirecionando para efetuar o Login.";
    header("refresh:3, URL=inicio.php");
  }else{
    echo "não foi possivel realizar o cadastro :(";
    header("refresh:3 URL=inicio.php");
  }
$start = "start.php";
    echo "<meta http-equiv=\"refresh\" content=\"3;URL=".$start."\">";
}

?>
