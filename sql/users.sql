-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Abr-2017 às 01:25
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `arquivo`
--

CREATE TABLE `arquivo` (
  `id` int(200) NOT NULL,
  `id_usuario` varchar(255) NOT NULL,
  `arquivo` varchar(200) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `arquivo`
--

INSERT INTO `arquivo` (`id`, `id_usuario`, `arquivo`, `data`) VALUES
(60, '169', '826dec0440fce82a20e1e7bf236a123f.jpg', '2017-04-02 03:34:28'),
(61, '171', '617e8e4aa3708f65bf51cbb0ebb42136.jpg', '2017-04-02 03:39:34'),
(62, '172', 'b87f3aef21145130ffe399c9592e307f.jpg', '2017-04-02 04:08:33'),
(63, '173', '6c21097d4d4fba27e8f5ef645ccab6b8.jpg', '2017-04-03 14:00:05'),
(64, '174', 'e6239cb78c9e0b24412e00109e124a8b.jpg', '2017-04-03 16:49:51'),
(65, '175', '7844ce09602e81d7fd6dc6b4d35fdc14.jpg', '2017-04-03 16:59:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imgcapa`
--

CREATE TABLE `imgcapa` (
  `id` int(11) NOT NULL,
  `id_usuario` varchar(255) NOT NULL,
  `capa` varchar(200) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `imgcapa`
--

INSERT INTO `imgcapa` (`id`, `id_usuario`, `capa`, `data`) VALUES
(50, '169', '7c5b950da05bfa7857eb32773b43d507.jpg', '2017-04-02 03:37:19'),
(54, '174', 'ce2d406cf867637f1d645a82da786e20.jpg', '2017-04-03 16:49:58'),
(53, '173', 'fca32c4a2c39fe832b6b9390eb774efa.jpg', '2017-04-03 14:00:11'),
(51, '171', '0126a81bfbb34b0c51941c64a549a004.jpg', '2017-04-02 03:39:44'),
(52, '172', '91e9b2c78f6abfb43ed22fb5605f23bb.jpg', '2017-04-02 04:08:40'),
(55, '175', '6925f3f7db5867b9137e98397f426250.jpg', '2017-04-03 16:59:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `id_usuario` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `curtidas` varchar(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(111) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `userlogin` longtext NOT NULL,
  `passlogin` longtext NOT NULL,
  `ativo` varchar(3) DEFAULT NULL,
  `start` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nome`, `email`, `userlogin`, `passlogin`, `ativo`, `start`) VALUES
(173, 'iris', 'iris@gmail.com1', '995f49f63e3385a9465351a4c4834d7d32a33466', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'sim', 0),
(172, 'andre corujao', 'andredeininger1@gmail.com', '6f2ecae9a16f69676ce45950bed9f47d6caccf0f', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'sim', 0),
(169, 'iris', 'iris@gmail.com', 'ab8cabeccb665bb0674f888ae4563e714d3944c0', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'sim', 1),
(171, 'fernando', 'fernandohlibka@gmail.coms', 'b2435f4d71f1c62e24e5c88e5a15c256a369e2b9', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'sim', 1),
(174, 'iris', 'iris@gmail.com11', '995f49f63e3385a9465351a4c4834d7d32a33466', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'sim', 0),
(175, 'iris ', 'iris@gmail.com111', '995f49f63e3385a9465351a4c4834d7d32a33466', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'sim', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arquivo`
--
ALTER TABLE `arquivo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imgcapa`
--
ALTER TABLE `imgcapa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arquivo`
--
ALTER TABLE `arquivo`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `imgcapa`
--
ALTER TABLE `imgcapa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
