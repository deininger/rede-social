﻿<?php
include "conexao.php";
session_start();

function logedUser(){
   return isset($_SESSION['emailUser']);
}

function logoUser(){
  if (!logedUser()){
    echo "Você não tem permissão para acessar essa página!";
    header("Location: inicio.php?error=acessonegado");
    die();
  }
}

function user(){
    $_SESSION['nomeUser'];
    $_SESSION['id'];
    $_SESSION['emailUser'];
}

function logout(){
    unset($_SESSION['nomeUser']);
    unset($_SESSION['id']);
    unset($_SESSION['idUser']);
    unset($_SESSION['emailUser']);
    unset($_SESSION['fotoC']);
    unset($_SESSION['idFoto']);
    unset($_SESSION['fotoUser']);
    session_destroy();
}

?>
