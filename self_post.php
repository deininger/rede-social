<style>
.publicacaohora {
	font-size: 10pt;
	margin-left: 15px;
	margin-top: 65px;
	position: relative;

}

.emoticons ul li {
	display: inline-block;
	list-style: none;

}

.emoticons input {
	display: inline;
	width: 20px;
	height: 20px;
	margin-left: -20px;
	float: left;
	padding: 15px;

}

.paragostei {
	margin-top: 15px;
	float: left;
	margin-left: -10px;
	color: #D71B1B;
	padding-right: 10px;

}

.paranomeusuario {
	float: left;
	margin-left: 10px;
	font-size: 11pt;
	margin-top: 25px;

}

.comentarios {
	width: 420px;
	height: 60px;
	float: left;
	margin-top: -20px;
	margin-left: 20px;
	border-top-style: none;
	border-right-style: none;
	border-radius: 0px 20px 20px 0px;
	opacity: 0.8;
	border-color: rgba(255,255,255,1.00);
	box-shadow: 3px 3px 2px 1px rgba(0,0,0,0.8);

}

.botaocomentario {
	float: right;
	width: 25px;
	height: 25px;
	margin-right: 3px;

}

.compartilhado {
	float: left;
	font-size: 10pt;
	margin-left: 15px;
	margin-top: -5px;

}

.usuarioperfilredondo {
	border-radius: 50%;
	float: left;
	width: 50px;
	height: 50px;
	margin-left: 10px;
	margin-top: 10px;
	box-shadow: 0px 0px 3px 1px #000;

}

.showcoment a{
	margin-left: 175px;
	color: #1342D8;
	margin-top: 50px;
	text-decoration: none;

}

.videotimeline {
	width: 450px;
	height: 400px;
	float: right;
	margin-top: -40px;
	margin-right: 15px;

}

.timelinevideofeedback {
	width: 480px;
	height: auto;
	float: right;
	margin-top: 10px;
	margin-right: 10px;
	margin-bottom: 10px;
	background: #F8F8FF;
	box-shadow: 5px 5px 3px 1px rgba(0,0,0,0.4);

}

.timelinetextfeedback {
	width: 480px;
	height: auto;
    float: right;
	margin-top: 10px;
	margin-right: 10px;
	margin-bottom: 10px;
	background: #F8F8FF;
	box-shadow: 5px 5px 3px 1px rgba(0,0,0,0.4);

}

.timelinetextpara {
	margin-top: -10px;
	float: left;
	margin-left: 5px;
	margin-right: 10px;
	border-top: 1px solid #A9A9A9;
	border-left: 1px solid #A9A9A9;
	border-top-left-radius: 3px;
	text-indent: 5px;
	text-align: justify;

}

.timelineimgfeedback {
	width: 480px;
	height: auto;
    float: right;
	margin-top: 10px;
	margin-right: 10px;
	margin-bottom: 10px;
	background: #F8F8FF;
	box-shadow: 5px 5px 3px 1px rgba(0,0,0,0.4);

}

.imgpost {
	width: 450px;
	height: 420px;
	float: right;
	margin-top: -10px;
	margin-right: 15px;


}

div#timelinepost {
	width: 500px;
	height: 190px;
	background: #E4E4E4;
	float:right;
	position: fixed;
	top: 63.8%;
	z-index: 1;
	display: none;
	opacity: 0.9;

}

div#timelinepost textarea {
	width: 420px;
	height: 60px;
	float: left;
	margin-top: 90px;
	margin-left: 20px;
	border-top-style: none;
	border-right-style: none;
	border-radius: 0px 20px 20px 0px;
	opacity: 0.4;
	-webkit-transition: 1s;
	border-color: rgba(255,255,255,1.00);
	box-shadow: 1px 1px 1px 1px black;
	position: absolute;
}

div#timelinepost textarea:hover {
	-webkit-transition: 1s;
	opacity: 1;
}

div#timelinebutton2 {
	width: 500px;
	height: 50px;
	position: fixed;
	background: rgba(252,252,252,0.40);
	top: 612px;
	margin-left: 19.5%;
	opacity: 0;
	-webkit-transition: 0.8s;
	z-index: 1

}

div#timelinebutton2:hover {
	opacity: 1;
	-webkit-transition: 0.8s;

}

div#timelinebutton2 img {
	width: 20px;
	height: 20px;
	position: fixed;
	top: 632px;
	margin-left: 16.8%;

}

</style>
<?php
  include "conexao.php";
  $text = $_POST['text'];
  $imgPost = isset($_FILES['imgpost'])?$_FILES['imgpost']: 0;
  var_dump($imgPost);
  $id = $_POST['id'];
  $img = 'nenhuma imagem';
  $curtidas = '0';
  if($imgPost == 1){
    $extensao = strtolower(substr($_FILES['imgpost']['name'], -4));
    $novo_nome = md5(time()) . $extensao;
    $diretorio = "upload/posts";

    move_uploaded_file($_FILES['imgpost']['tmp_name'], $diretorio.$novo_nome);

    $postQ = mysqli_query($conexao, "INSERT INTO posts (id, id_usuario, texto, img, data, curtidas) VALUES ( '{$id}', '{$text}', '{$novo_nome}', NOW(), '{$curtidas}')");
    $post = mysqli_query($conexao, $postQ);
    if($post === false){
      mysqli_error();
    }


  }else{
    $postQ = "INSERT INTO posts (id_usuario, texto, img, data, curtidas) VALUES ('{$id}', '{$text}', '{$img}', NOW(), '{$curtidas}')";
    $post = mysqli_query($conexao, $postQ);
    if($post === false){
      mysqli_error();
    }
  }

function mostraPosts($conexao, $id){
    $mostraPost = mysqli_query($conexao, "SELECT * FROM posts WHERE id_usuario = '{$id}'");
    while($mostraPostr = mysqli_fetch_assoc($mostraPost)){
      echo "<div class='timelinevideofeedback'>
      	<img class='usuarioperfilredondo' src='upload/$_SESSION[fotoC]'/><p class='paranomeusuario'>$_SESSION[nomeUser]</p>
      	<p class='publicacaohora'>Publicado agora</p>
        <p class='timelinetextpara'>$mostraPostr[texto]</p>
      <nav class='emoticons'>
      	<ul>
      		<li><input alt='image' type='image' src='_imagens/Iconeco.png'/><p class='paragostei'>$mostraPostr[curtidas]</p></li>
      		<li><input type='image' alt='image' src='_imagens/iconecompartilhar.png'/></li>
      		<li><input alt='image' type='image' src='_imagens/mensagemprivadadireta.png'/</li>
      	</ul>
      	</nav>
      	<textarea class='comentarios' placeholder='Comente sobre isso...' maxlength='999999999' type='text'></textarea><input class='botaocomentario' alt='image' type='image' src='_imagens/comentarioicone.png'/>
      	<a href='#'><p class='showcoment'>comentarios</p></a>
      	</div>";
    }

}

?>
